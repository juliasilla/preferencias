/*

Este ejemplo se ha obtenido modificando el código del tutorial:
http://www.nosinmiubuntu.com/como-guardar-datos-en-android/

 */

package com.example.masilmar.preferencias;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    //Definimos las variables para los 3 controles
    private Button btnEmail;
    private TextView lblEmail;
    private EditText txtEmail;
    //Definimos una variable para el objeto de tipo configuracion
    private Preferencias conf;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Instanciamos los controles y el objeto configuración
        btnEmail = (Button) findViewById(R.id.btnEmail);
        lblEmail = (TextView) findViewById(R.id.lblEmail);
        txtEmail = (EditText) findViewById(R.id.txtEmail);
        conf = new Preferencias(this);

        //Escribimos el valor del email guardado anteriormente, si existe dicho valor
        if (conf.getUserEmail() != null)
            //lblEmail.setText(conf.getUserEmail());
            txtEmail.setText(conf.getUserEmail());


        //Definimos el evento click del botón para poder guardar el nuevo email
        btnEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                conf.setUserEmail(txtEmail.getText().toString());
                goToSecondView();

            }
        });
    }
    public void goToSecondView(){
        Intent i = new Intent(this, SecondActivity.class);
        startActivity(i);
    }

}

