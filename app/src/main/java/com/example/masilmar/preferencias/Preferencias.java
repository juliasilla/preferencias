package com.example.masilmar.preferencias;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by masilmar on 24/10/16.
 */
public class Preferencias {

    private final String SHARED_PREFS_FILE = "FilePrefs";
    private final String KEY_EMAIL = "email";

    private Context mContext;





    public Preferencias(Context context){
        mContext = context;
    }


    private SharedPreferences getSettings(){
        return mContext.getSharedPreferences(SHARED_PREFS_FILE, 0);
    }

    public String getUserEmail(){
        return getSettings().getString(KEY_EMAIL, null);
    }

    public void setUserEmail(String email){
        SharedPreferences.Editor editor = getSettings().edit();
        editor.putString(KEY_EMAIL, email );
        editor.commit();
    }

}
